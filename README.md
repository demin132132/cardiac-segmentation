# Cardiac Segmentation


## Getting started

Репозиторий содержит код для обучения модели Unet сегментации мрт изображений сердца

Модель (папка imATFIB-master) взята из https://github.com/RonaldGalea/imATFIB 

Предобученная модель по адресу:

Данные https://www.doc.ic.ac.uk/~rkarim/la_lv_framework/fibrosis/

Для запуска обучения поместите данные (папку 'pre' и папку 'post') в корневую папку проекта,
установите зависимости из requirements.txt и запустите блокнот Cardiac_Segmentation.ipynb.

Для тестового запуска запустите блокнот inference.ipynb
