import numpy as np
import h5py
import scipy
#f = h5py.File('/home/gildor/ITMO/Сardiology/mrimages/sol_yxzt_pat17.mat','r')
#data = f.get('data/variable1')
#data = np.array(data) # For converting to a NumPy array
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib as mpl
#import matplotlib.pyplot as plt
#from colorspacious import cspace_converter
import io
import os
import torch


import scipy.io
mat_xyzt = scipy.io.loadmat('/home/gildor/ITMO/Сardiology/mrimages/sol_yxzt_pat18.mat')
mat_seg = scipy.io.loadmat('/home/gildor/ITMO/Сardiology/manual_seg/manual_seg_32points_pat18.mat')


def get_one_image(sol_yxzt, z, time):
    return sol_yxzt[:, :, z, time]

#print(mat_xyzt['sol_yxzt'][:, :, 0, 0].shape)
#fig = plt.imshow(mat_xyzt['sol_yxzt'][:, :, 0, 0], cmap='gist_gray')
#plt.gca().set_axis_off()
#plt.savefig('../fig/pl_18.png', dpi=256, bbox_inches="tight", pad_inches=0.0)


for part in range(3, 34):
    mat_xyzt = scipy.io.loadmat(
        f'/home/gildor/ITMO/Сardiology/mrimages/sol_yxzt_pat{part}.mat'
    )
    for z in range(0, 10):
        z_dir = f'../fig/z_{z}/'
        if not os.path.isdir(z_dir):
            os.mkdir(z_dir)
        for t in range(0, 20):
            print(f'part:{part} z: {z} time: {t}')
            save_dir = f'../fig/z_{z}/time_{t}/'
            if not os.path.isdir(save_dir):
                os.mkdir(save_dir)
            fig = plt.imshow(get_one_image(mat_xyzt['sol_yxzt'], z, t), cmap='gist_gray', vmin=0, vmax=500,)
            plt.gca().set_axis_off()
            plt.savefig(save_dir + f'part_{part}_z{z}_t{t}.png', dpi=256, bbox_inches="tight", pad_inches=0.0)





